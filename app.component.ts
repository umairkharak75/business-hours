import { Component, OnInit } from '@angular/core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { FormGroup, FormControl, FormBuilder, FormArray, AbstractControl } from '@angular/forms';
import * as moment from 'moment';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor(public fb:FormBuilder){}
sundayForm
mondayForm
tuesdayForm
wednesdayForm
thursdayForm
fridayForm
saturdayForm
hasTimeError
myProperty
difference_in_milliseconds
  ngOnInit(){
    this.sundayForm = this.fb.group({
      sunday: this.fb.array([])
    })
    this.mondayForm = this.fb.group({
      monday: this.fb.array([])
    })
    this.tuesdayForm = this.fb.group({
      tuesday: this.fb.array([])
    })
    this.wednesdayForm = this.fb.group({
      wednesday: this.fb.array([])
    })
    this.thursdayForm = this.fb.group({
      thursday: this.fb.array([])
    })
    this.fridayForm = this.fb.group({
    friday: this.fb.array([])
    })
    this.saturdayForm = this.fb.group({
      saturday: this.fb.array([])
    })

  this.addSundayTime()
  this.addMondayTime()
  this.addTuesdayTime()
  this.addSaturdayTime()
  this.addFridayTime()
  this.addThursdayTime()
  this.addWednesdayTime()

    
      


}

  get getSundayTime() {
    return this.sundayForm.get('sunday') as FormArray
  }
  get getmondayTime() {
    return this.mondayForm.get('monday') as FormArray
  }
  get getTuesdayTime() {
    return this.tuesdayForm.get('tuesday') as FormArray
  }
  get getWednesdayTime() {
    return this.wednesdayForm.get('wednesday') as FormArray
  }
  get getThursdayTime () {
    return this.thursdayForm.get('thursday') as FormArray
  }
  get getFridayTime() {
    return this.fridayForm.get('friday') as FormArray
  }
  get getSaturdayTime() {
    return this.saturdayForm.get('saturday') as FormArray
  }
    
  addSundayTime() {
    this.getSundayTime.push(this.createTimeObject());
  }
  addMondayTime() {
    this.getmondayTime.push(this.createTimeObject());
  }
  addTuesdayTime() {
    this.getTuesdayTime.push(this.createTimeObject());
  }
  addWednesdayTime() {
    this.getWednesdayTime.push(this.createTimeObject());
  }
  addThursdayTime() {
    this.getThursdayTime.push(this.createTimeObject());
  }
  addFridayTime() {
    this.getFridayTime.push(this.createTimeObject());
  }
  addSaturdayTime() {
    this.getSaturdayTime.push(this.createTimeObject());
  }
  
  deleteSundayTime(i) {
    this.getSundayTime.removeAt(i)
  }
  deleteMondayTime(i) {
    this.getmondayTime.removeAt(i)
  }
  deleteTuesdayTime(i) {
    this.getTuesdayTime.removeAt(i)
  }
  deleteWednesdayTime(i) {
    this.getWednesdayTime.removeAt(i)
  }
  deleteThursdayTime(i) {
    this.getThursdayTime.removeAt(i)
  }
  deleteFridayTime(i) {
    this.getFridayTime.removeAt(i)
  }
  deleteSaturdayTime(i) {
    this.getSaturdayTime.removeAt(i)
  }

  createTimeObject(){
    const timeObj = this.fb.group({ 
      startingTime: [],
      endingTime: [],
    })
    return timeObj
  }

  
 

}